const fs = require('fs');

module.exports = (path) => {
  let fileData = [];

  const parseAllFiles = function(files) {
    return Promise.all(files.map(file => {
      return new Promise((done, fail) => {
        fs.readFile(file, {encoding: 'utf8'}, (error, data) => {
          if (error) {
            fail(error);
          } else {
            fileData.push({name: file, content : data});
            done(data);
          }
        });
      });
    }));
  };

  return new Promise((done, fail) => {
    fs.readdir(path, (error, files) => {

      if (error) {
        fail(error);
      } else {
        parseAllFiles(files).then(() => {
          done(fileData);
        }).catch(error => fail(error));
      }
    });
  });
};