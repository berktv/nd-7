const fs = require('fs');

module.exports = (path, callback) => {
  fs.stat(path, (err, stats) => {
    if (err) return callback(err);

    if (stats.isFile()) {
      fs.readFile(path, {encoding: 'utf8'}, (err, content) => {
        if (err) return callback(err);

        callback(null, {
          path: path,
          type: 'file',
          content: content
        });
      });
    }

    if (stats.isDirectory()) {
      fs.readdir(path, {encoding: 'utf8'}, (err, files) => {
        if (err) return callback(err);

        callback(null, {
          path: path,
          type: 'directory',
          childs: files
        });
      });
    }
  });
};