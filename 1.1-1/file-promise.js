const fs = require('fs');

const read = (file) => {
	return new Promise((done, fail) => {
		fs.readFile(file, {encoding: 'utf8'}, (err, data) => {
			if (err) {
				fail(err);
			} else {
				done(data);
			}
		});
	});
};

const write = (file, data) => {
	return new Promise((done, fail) => {
		fs.writeFile(file, data, {encoding: 'utf8'}, err => {
			if (err) {
				fail(err);
			} else {
				done(file);
			}
		});
	});
};

module.exports = {read, write};